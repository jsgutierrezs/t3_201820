package test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import model.data_structures.DoubleLinkedList;

public class DoublyLinkedListTest {

private DoubleLinkedList<Integer> lista;
	
	@Test
	public void funciones()
	{
		lista = new DoubleLinkedList<Integer>();
		try
		{
			lista.add(1);
			assertTrue(lista.getFirst()==1);
			assertTrue(lista.getSize()==1);

			lista.add(4);
			lista.add(7);
			lista.add(3);

			assertTrue(lista.getSize() == 4);
			assertTrue(lista.getObject(3)==3);
			assertTrue(lista.getObject(1)==4);

			lista.addAt(2, 4);
			assertTrue(lista.getSize()==5);
			assertTrue(lista.getObject(4)==2);

			lista.addAt(2, 2);
			assertTrue(lista.getSize()==6);
			assertTrue(lista.getObject(2)==2);

			lista.addAt(10, 0);
			assertTrue(lista.getFirst()==10);
			assertTrue(lista.getSize()==7);
			
			lista.delete(10);
			assertFalse(lista.getFirst()==10);
			assertTrue(lista.getSize()==6);
			lista.delete(2);
			assertFalse(lista.getObject(2)==2);
			assertTrue(lista.getSize()==5);

			lista.delete(2);
			assertFalse(lista.getObject(3)==2);
			assertTrue(lista.getSize()==4);
			
			lista.add(6);
			lista.add(8);
			lista.add(19);
			
			lista.deleteAt(0);
			
			assertTrue(lista.getFirst()==4);
			assertTrue(lista.getSize()==6);
			
			lista.deleteAt(5);
			
			assertTrue(lista.getObject(4)==8);
			assertTrue(lista.getSize()==5);
			
			lista.deleteAt(2);
			
			assertTrue(lista.getObject(2)==6);
			assertTrue(lista.getSize()==4);
			
		}catch(Exception e){
			e.printStackTrace();
			fail("No debe generar excepcion");
		}	
	}
	
	@Test
	public void iterator()
	{
		DoubleLinkedList<Integer> prueba = new DoubleLinkedList<>();
		prueba.add(1);
		Iterator<Integer> iter = prueba.iterator();
		assertTrue(iter.hasNext());
		try
		{
			prueba.deleteAt(0);
		}catch(Exception e)
		{
			
		}
		for(int i = 0; i < 10; i++)
		{
			prueba.add(i);
		}
		iter = prueba.iterator();
		int p = 0;
		while(iter.hasNext())
		{
			int i = iter.next();
			assertTrue( "el valor "+ p + "no es igual al valor" + i, p == i);
			p ++;
		}
	}


}
