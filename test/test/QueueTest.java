package test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import model.data_structures.Queue;

public class QueueTest {

private Queue<Integer> queue;
	
	@Test
	public void funciones()
	{
		
		
		queue = new Queue<Integer>();
		try
		{

			queue.enqueue(4);
			queue.enqueue(7);
			queue.enqueue(3);

			assertTrue(queue.size() == 3);
			assertTrue(queue.dequeue()==4);
			assertTrue(queue.dequeue()==7);

			
		}catch(Exception e){
			e.printStackTrace();
			fail("No debe generar excepcion");
		}	
	}
	
	@Test
	public void iterator()
	{

	}

}
