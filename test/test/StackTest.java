package test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import model.data_structures.Stack;

public class StackTest {

private Stack<Integer> stack;
	
	@Test
	public void funciones()
	{
		
		
		stack = new Stack<Integer>();
		try
		{
			stack.push(1);
			assertTrue(stack.size() == 1);
			assertTrue(stack.pop()==1);
			assertTrue(stack.size()==0);

			stack.push(4);
			stack.push(7);
			stack.push(3);

			assertTrue(stack.size() == 3);
			assertTrue(stack.pop()==3);
			assertTrue(stack.pop()==7);

			
		}catch(Exception e){
			e.printStackTrace();
			fail("No debe generar excepcion");
		}	
	}
	
	@Test
	public void iterator()
	{

	}
}
