package model.data_structures;

public class Node<T> {

	private Node<T> next;
	
	private T item;
	
	public Node(T t) {
		
		this.item = t;
		
	}
	
	public Node getNext() {
		
		return next;
				
	}
	
	public T getItem() {
		
		return item;
		
	}
	
	
	public void setNext(Node pNext) {
		
		next = pNext;
	}
	
	
}
