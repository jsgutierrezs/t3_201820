package model.data_structures;

import java.util.Iterator;

public class IteratorDouble<T> implements Iterator<T>{

	private NodeDoubly<T> actual;

	private int cont;

	public IteratorDouble(NodeDoubly<T> First)
	{
		actual = First;
		cont = 0;
	}
	@Override
	public boolean hasNext() {
		if(actual!= null)
		{
			return actual.getNext()!=null || cont == 0;
		}
		return false;
	}

	@Override
	public T next(){
		if(cont == 0)
		{
			cont ++;
		}else
		{
			actual = actual.getNext();
		}
		return actual.getObject();
	}
}
