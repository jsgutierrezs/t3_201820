package model.data_structures;

import java.util.Iterator;

/**
 * This class creates a doubly linked list of the objects that enters as parameters
 * @param <T> the type of object that enters as parameter
 */
public class DoubleLinkedList<T> implements IDoublyLinkedList<T> {
	
	/**
	 * it represents the first element in the array
	 */
	private NodeDoubly<T> First;
	
	/**
	 * the amount of elements in the list
	 */
	private int size;
	
	
	public DoubleLinkedList() 
	{
		size = 0;
		First = null;
	}

	@Override
	public Iterator<T> iterator() {
		return new IteratorDouble<T>(First);
	}

	@Override
	public Integer getSize() {
		return size;
	}

	@Override
	public T getFirst() {
		return First.getObject();
	}

	@Override
	public T getObject(int posicion) {
		NodeDoubly<T> n = First;
		for (int i = 1; i <= posicion; i++) {
			n = n.getNext();
		}
		return n.getObject();
	}

	@Override
	public void add(T aAgregar) {
		NodeDoubly<T> n = First;
		if(First == null)
		{
			First = new NodeDoubly<T>(aAgregar);
			size ++;
			return;
		}
		else
		{
			while(n.getNext()!=null)
			{
				n = n.getNext();
			}
			NodeDoubly<T> a = new NodeDoubly<T>(aAgregar);
			a.changePrevious(n);
			n.changeNext(a);
			size ++;
		}
	}

	@Override
	public void addAt(T aAgregar, int posicion) {
		NodeDoubly<T> actual = First;
		NodeDoubly<T> agr = new NodeDoubly<T>(aAgregar);
		if(posicion == 0)
		{
			First = new NodeDoubly<T>(aAgregar);
			First.changeNext(actual);
			size ++;
		}else if(posicion == size)
		{
			add(aAgregar);
			return;
		}else if(posicion < size)
		{
			int i=1;
			while (i <= posicion) {
				actual = actual.getNext();
				i++;
			}
			NodeDoubly<T> ant = actual.getPrevious();
			agr.changePrevious(ant);
			agr.changeNext(actual);
			ant.changeNext(agr);
			actual.changePrevious(agr);
			size ++;
		}

	}

	@Override
	public void delete(T aEliminar) throws Exception{
		NodeDoubly<T> aElim = First;
		if(First.getObject().equals(aEliminar))
		{
			First = aElim.getNext();
			aElim.changePrevious(null);
			size--;
		}else
		{
			boolean b = false;
			while(aElim != null && !b)
			{
				aElim = aElim.getNext();
				b = aElim.getObject().equals(aEliminar)? true:false;
			}
			if(aElim == null)
			{
				throw new Exception ("Error");
			}
				NodeDoubly<T> ant = aElim.getPrevious();
				NodeDoubly<T> sig = aElim.getNext();
				ant.changeNext(sig);
				if(sig != null)
				{
					sig.changePrevious(ant);
				}
				size--;
		}
	}

	@Override
	public void deleteAt(int posicion) throws Exception {
		NodeDoubly<T> act = First;
		if(posicion >= size && posicion < 0)
		{
			throw new Exception ("Introducir una posicion que se encuentre en la lista");
		}
		if(posicion == 0)
		{
			First = First.getNext();
			First.changePrevious(null);
			size--;
		}
		else
		{
			for (int i = 1; i <= posicion; i++) {
				act = act.getNext();
			}
			NodeDoubly<T> ant = act.getPrevious();
			if(act.getNext()!=null)
			{
				act.getNext().changePrevious(ant);
			}
			ant.changeNext(act.getNext());
			size--;
		}
	}
}
