package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T> {

	private Node<T> top;
	
	private int size;
	
	public Stack()
	{
		top = null;
		size = 0;
	}
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		return size != 0;
	}

	@Override
	public int size() {
		return size;
	}

	public Node<T> getTop() {
		return top;
	}
	@Override
	public void push(T t) {
		Node<T> nodo = top;
		top = new Node<T>(t);
		top.setNext(nodo);
		size++;
	}

	@Override
	public T pop() {
		Node<T> nodo = top;
		top = top.getNext();
		size--;
		return nodo.getItem();
	}

}
