package model.data_structures;

import java.util.Iterator;


public class Queue<T> implements IQueue<T> {
	
	private Node<T> first;
	
	private Node<T> last;
	
	private int size;

	public Queue() {
		first = null;
		last = null;
		size = 0;
	}
	@Override
	public Iterator<T> iterator() {
		return new TheIterator<T>(first);
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(first == null) return true;
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}


	@Override
	public T dequeue() {
		size--;
		if(first.getNext()!= null)
		{
			Node<T> toReturn = first;
			first = first.getNext();
			return toReturn.getItem();
		}
		return null;
	}
	@Override
	public void enqueue(T t) {
		// TODO Auto-generated method stub
		Node<T> n = new Node<T>(t);
		if(first == null) {
			first = n;
			last = n;
			size++;
			return;
		}
		last.setNext(n);
		last = n;
		size++;
	}

}
