package model.data_structures;

import java.util.Iterator;

public class TheIterator<T> implements Iterator<T> {
	
	private Node<T> actual;
	
	private int cont;
	
	public TheIterator(Node<T> first)
	{
		actual = first;
		cont = 0;
	}
	@Override
	public boolean hasNext() {
		if(actual!= null)
		{
			return actual.getNext()!=null || cont == 0;
		}
		return false;
	}

	@Override
	public T next(){
		if(cont == 0)
		{
			cont ++;
		}else
		{
			actual = actual.getNext();
		}
		return actual.getItem();
	}

	
}
