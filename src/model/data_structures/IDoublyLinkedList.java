package model.data_structures;

import java.util.Iterator;

public interface IDoublyLinkedList<T> extends Iterable<T> {
	/**
	 * Returns the size of the list
	 * @return size of the list
	 */
	Integer getSize();

	/**
	 * Returns the first element in the linked list
	 * @return first element in the linked list
	 */
	T getFirst();
	
	/**
	 * Returns the object found at the position in the parameter
	 * @param posicion position of the object in the list. 
	 * @return the object in the position position
	 * @throws exception if the position is not between 0 and the size
	 */
	T getObject(int position) throws Exception;
	
	/**
	 * It adds an object at the end of the list
	 * <b>post: </b> the element was added at the end of the list
	 * @param add the element that is going to be added
	 */
	void add(T add);
	
	/**
	 * It adds an object at the position in the parameter
	 * <b>post: </b> the element was added
	 * @param add the element that is going to be added
	 * @param position position of the new object in the list
	 * @throws exception if the position is not between 0 and the size
	 */
	void addAt(T add, int position);
	
	/**
	 * It deletes the element from the list that is equals to the element in the parameter
	 * <b>post: </b> the element was deleted if it existed. else it throws an exception
	 * @param delete the element that is going to be deleted
	 * @throws Exception if the element is not found
	 */
	void delete(T delete) throws Exception;
	
	/**
	 * It deletes the object in the position of the parameter
	 *  <b>post: </b> the element was deleted.
	 * @param position position of the element in the list
	 * @throws an exception if the parameter isn't between 0 and the size
	 */
	void deleteAt(int position) throws Exception;
}
