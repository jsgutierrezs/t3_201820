package model.data_structures;


/**
 * Represents one NodeDoubly of the linked list 
 * @param <T> type of object
 */
public class NodeDoubly<T>{
	
	/**
	 * Object inside the NodeDoubly
	 */
	private T object;
	
	/**
	 * Previous NodeDoubly in the linked list
	 */
	private NodeDoubly<T> previousNodeDoubly;
	
	/**
	 * Next NodeDoubly in the linked list
	 */
	private NodeDoubly<T> nextNodeDoubly;
	
	/**
	 * It creates a NodeDoubly with the object in the parameter
	 * @param object object inside the list. object != null
	 */
	public NodeDoubly(T object)
	{
		this.object = object;
	}
	
	/**
	 * It returns the previous NodeDoubly
	 * @return previous NodeDoubly in the list
	 */
	public NodeDoubly<T> getPrevious()
	{
		return previousNodeDoubly;
	}
	
	/**
	 * It returns the next NodeDoubly in the list
	 * @return next NodeDoubly in the list
	 */
	public NodeDoubly<T> getNext()
	{
		return nextNodeDoubly;
	}
	
	/**
	 * Returns the object in the NodeDoubly
	 * @return object in the NodeDoubly
	 */
	public T getObject()
	{
		return object;
	}
	
	/**
	 * It changes the next NodeDoubly by the one in the parameter
	 * @param obj NodeDoubly that is going to become the next
	 */
	public void changeNext(NodeDoubly<T> obj)
	{
		nextNodeDoubly = obj;
	}
	
	/**
	 * It changes the previous NodeDoubly by the one in the parameter
	 * @param obj NodeDoubly that is going to become the previous
	 */
	public void changePrevious(NodeDoubly<T> obj)
	{
		previousNodeDoubly = obj;
	}
}
