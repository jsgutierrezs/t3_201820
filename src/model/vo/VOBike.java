package model.vo;

public class VOBike {

	private int id;
	
	private String name;
	
	private String city;
	
	private double latitude;
	
	private double longitude;
	
	private int dCapacity;
	
	private String onlineDate;

	

	public VOBike(int id, String name, String city, double latitude, double longitude, int dCapacity,
			String onlineDate) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dCapacity = dCapacity;
		this.onlineDate = onlineDate;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCity() {
		return city;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public int getdCapacity() {
		return dCapacity;
	}

	public String getOnlineDate() {
		return onlineDate;
	}

	
}
