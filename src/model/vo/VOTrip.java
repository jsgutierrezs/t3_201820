package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	//trip_id,start_time,end_time,bikeid,tripduration,from_station_id,from_station_name,to
	//_station_id,to_station_name,usertype,gender,birthyear.
	
	private int tripId;
	
	private String startTime;
	
	private String endTime;
	
	private String bikeId;
	
	private String tripDuration;
	
	private String fromStationId;
	
	private String fromStationName;
	
	private String toStationId;
	
	private String toStationName;
	
	private String userType;
	
	private String gender;
	
	private String birthYear;
	
	public VOTrip(int pTripId, String pStartTime, String pEndTime, String pBikeId, String pTripDuration, String pFromId, String pFromName, 
			String pToId, String pToName, String pUser, String pGender, String pBirth) {
		tripId = pTripId;
		startTime = pStartTime;
		endTime = pEndTime;
		bikeId = pBikeId;
		tripDuration = pTripDuration;
		fromStationId = pFromId;
		fromStationName = pFromName;
		toStationId = pToId;
		toStationName = pToName;
		userType = pUser;
		gender = pGender;
		birthYear = pBirth;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public int getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	public String getStartTime() {
		return startTime;
	}
	
	public String getEndTime() {
		return endTime;
	}
	
	public String getBikeId() {
		return bikeId;
	}
	
	public String getTripDuration() {
		return tripDuration;
	}
	
	public String getFromStationId() {
		return fromStationId;
	}
	
	public String getFromStationName() {
		return fromStationName;
	}
	
	public String getToStationId() {
		return toStationId;
	}
	
	public String getToStationName() {
		return toStationName;
	}
	
	public String getUserType() {
		return userType;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String getBirthYear() {
		return birthYear;
	}
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return "";
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return "";
	}
}
