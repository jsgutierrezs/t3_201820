package model.logic;

import java.io.*;
import java.io.IOException;

import api.IDivvyTripsManager;
import model.vo.*;
import model.data_structures.*;

public class DivvyTripsManager implements IDivvyTripsManager {

	private Queue<VOTrip> queue1;
	
	private Stack<VOTrip> stack1;

	private DoubleLinkedList<VOBike> stationList;
	public void loadStations (String stationsFile) {	
		try {
			stationList = new DoubleLinkedList<>();
			FileReader lector = new FileReader(stationsFile);
			BufferedReader bfLector = new BufferedReader(lector);
			String linea = bfLector.readLine();
			linea = bfLector.readLine();
			while(linea != null)
			{
				String[] separado = linea.split(",");
				VOBike v = new VOBike(Integer.parseInt(separado[0]), separado[1], separado[2], Double.parseDouble(separado[3]), Double.parseDouble(separado[4]), Integer.parseInt(separado[5]), separado[6]);
				stationList.addAt(v, 0);
				linea = bfLector.readLine();
			}
			
			bfLector.close();
			lector.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadTrips (String tripsFile) {
		try
		{
			queue1 = new Queue<>();
			stack1 = new Stack<>();
			FileReader reader = new FileReader(tripsFile);
			BufferedReader bfReader = new BufferedReader(reader);
			String linea = bfReader.readLine();
			linea = bfReader.readLine();
			int c = 0;
			while(linea != null) {
				String[] q = linea.split(",");
				String q10 = q[10]!=""? q[10].substring(1, q[10].length()-1): "";
				String q11 = q[11]!=""? q[11].substring(1, q[11].length()-1): "";
				VOTrip k = new VOTrip(Integer.parseInt(q[0].substring(1, q[0].length()-1)), q[1].substring(1, q[1].length()-1),
									q[2].substring(1, q[2].length()-1), q[3].substring(1, q[3].length()-1),
									q[4].substring(1, q[4].length()-1), q[5].substring(1, q[5].length()-1), 
									q[6].substring(1, q[6].length()-1), q[7].substring(1, q[7].length()-1), 
									q[8].substring(1, q[8].length()-1), q[9].substring(1, q[9].length()-1), 
									q10, q11);
				queue1.enqueue(k);
				stack1.push(k);
				//System.out.println(c);
				//c++;
				linea = bfReader.readLine();
			}
			bfReader.close();
			reader.close();
		}catch(IOException e)
		{
			System.out.println("ERROR io");
		}
	}
	
	@Override
	public DoubleLinkedList <String> getLastNStations (int bicycleId, int n) {
		DoubleLinkedList<String> res = new DoubleLinkedList<>();
		VOTrip tmp = queue1.dequeue();
		int c = 0;
		while(res.getSize() != n && 0!=queue1.size()) {
			if( Integer.parseInt(tmp.getBikeId())==bicycleId && c < n) {
				String temp = tmp.getToStationName();
				res.add(temp);
				c++;
			}
			tmp = queue1.dequeue();
		}
		return res;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		VOTrip tmp = stack1.pop();
		VOTrip resp = null;
		int c = 0;
		int i = 0;
		while(resp == null && i<stack1.size()) {
			if(Integer.parseInt(tmp.getToStationId()) == stationID ) {
				c++;
				resp = c==n? tmp: null;
			}
			tmp = stack1.pop();
		}
		return resp;
	}	


}
